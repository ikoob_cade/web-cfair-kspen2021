import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';
import { Router } from '@angular/router';


enum AUTH_ERROR_MESSAGE {
  UNAUTHORIZED = 'Login information does not match.',
  NOT_FOUND = 'E-mail could not be found.',
  ENTER_ALL = 'Please enter all values.'
}
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {
  @ViewChild('policyAlertBtn') policyAlertBtn: ElementRef;
  private user: any;
  public passwordError = ''; // 로그인 에러메세지
  public member = {
    email: '',
    password: '',
  };

  constructor(
    public router: Router,
    public auth: AuthService) { }

  ngOnInit(): void {
  }

  login(): void {
    if (this.loginValidator()) {
      this.auth.login(this.member).subscribe(res => {
        if (res.token) {
          this.user = res;
          this.passwordError = '';
          // this.policyAlertBtn.nativeElement.click();
          this.goToMain();
        }
      }, error => {
        if (error.status === 401) {
          this.passwordError = AUTH_ERROR_MESSAGE.UNAUTHORIZED;
        }
        if (error.status === 404) {
          this.passwordError = AUTH_ERROR_MESSAGE.NOT_FOUND;
        }
      });
    } else {
      alert(AUTH_ERROR_MESSAGE.ENTER_ALL);
    }
  }

  loginValidator(): boolean {
    if (!this.member.email || !this.member.password) {
      return false;
    }
    return true;
  }

  /**
   * 로그인 정보를 스토리지 저장 및 메인으로 이동.
   */
  goToMain(): void {
    sessionStorage.setItem('cfair', JSON.stringify(this.user));
    // sessionStorage.setItem('cfair', JSON.stringify(this.user));
    this.router.navigate(['/main']);
  }
}
