import { Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { VodService } from '../../../services/api/vod.service';
import { NgImageSliderComponent } from 'ng-image-slider';
import { BannerService } from '../../../services/api/banner.service';
import { MemberService } from '../../../services/api/member.service';
import { HistoryService } from '../../../services/api/history.service';

@Component({
  selector: 'app-vod-detail',
  templateUrl: './vod-detail.component.html',
  styleUrls: ['./vod-detail.component.scss']
})
export class VodDetailComponent implements OnInit {
  @ViewChild('bannerSlider') bannerSlider: NgImageSliderComponent;
  @ViewChild('entryAlertBtn') entryAlertBtn: ElementRef;

  public vod: any;
  public vodId: string;
  public banners = [];
  private user;
  public next: any;

  constructor(
    private location: Location,
    private activatedRoute: ActivatedRoute,
    public vodService: VodService,
    private bannerService: BannerService,
    private memberService: MemberService,
    private historyService: HistoryService,
  ) { }

  ngOnInit(): void {
    this.historyService.setAttendance('in');
    this.user = JSON.parse(sessionStorage.getItem('cfair'));

    this.activatedRoute.params.subscribe(params => {
      if (params.vodId) {
        this.vodService.findOne(params.vodId).subscribe(vod => {
          this.vod = vod.contents;
          this.vodId = vod.id;
        });
        this.getBanners();
      }
    });
  }

  /** 배너목록을 조회한다. */
  getBanners(): void {
    this.bannerService.find().subscribe(res => {
      res.vod.forEach(item => {
        const data = {
          link: item.link,
          thumbImage: item.photoUrl,
          alt: item.title,
        };
        this.banners.push(data);
      });
    });
  }

  slidePrev(target): void {
    this[target].prev();
  }
  slideNext(target): void {
    this[target].next();
  }

  imageClick(index): void {
    if (this.banners[index] && this.banners[index].link) {
      window.open(this.banners[index].link);
    }
  }

  // 리스트로 돌아가기
  goBack(): void {
    this.location.back();
  }

}
